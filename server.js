const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2/promise');
const moment = require('moment');
const app = express();
const port = 8000;

const dbConfig = {
    host: '188.166.215.35',
    user: 'developer',
    password: 'Root@1234',
    database: 'top_phayao'
};

app.use(bodyParser.json());

app.get('/shops', async(req, res) => {
    try {
        const connection = await mysql.createConnection(dbConfig);
        const [results] = await connection.query('SELECT * FROM shops');
        connection.end();

        res.json(results);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

app.post('/shops', async(req, res) => {
    try {
        const connection = await mysql.createConnection(dbConfig);
        const [results] = await connection.query('INSERT INTO shops (name,description,created_at,updated_at) VALUES (?,?,?,?)', [
            req.body.name,
            req.body.description,
            moment().format('YYYY-MM-DD HH:mm:ss'),
            moment().format('YYYY-MM-DD HH:mm:ss')
        ]);
        connection.end();

        res.json({ id: results.insertId, name: req.body.name });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

app.get('/shops/:id', async(req, res) => {
    try {
        const connection = await mysql.createConnection(dbConfig);
        const [results] = await connection.query('SELECT * FROM shops WHERE id = ?', [req.params.id]);
        connection.end();

        if (results.length === 0) {
            return res.status(404).json({ error: 'Item not found' });
        }

        res.json(results[0]);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

app.put('/shops/:id', async(req, res) => {
    try {
        const connection = await mysql.createConnection(dbConfig);
        await connection.query('UPDATE shops SET name = ?, description = ?, updated_at = ? WHERE id = ?', [
            req.body.name,
            req.body.description,
            moment().format('YYYY-MM-DD HH:mm:ss'),
            req.params.id
        ]);
        connection.end();

        res.json({ id: req.params.id, name: req.body.name });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

app.delete('/shops/:id', async(req, res) => {
    try {
        const connection = await mysql.createConnection(dbConfig);
        await connection.query('DELETE FROM shops WHERE id = ?', [req.params.id]);
        connection.end();

        res.json({ message: 'Delete is success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});


app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});